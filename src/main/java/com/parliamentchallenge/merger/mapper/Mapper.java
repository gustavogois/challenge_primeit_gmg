package com.parliamentchallenge.merger.mapper;

import com.parliamentchallenge.merger.service.model.Member;
import com.parliamentchallenge.merger.service.model.Speech;
import com.parliamentchallenge.merger.dto.SpeechDTO;
import com.parliamentchallenge.merger.util.MemberHelper;
import org.springframework.hateoas.Link;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Mapper {

    public static List<SpeechDTO> speechesAndMembersToSpeechesDTO(List<Speech> speeches, Map<String,
            Member> membersSpeech) {

        List<SpeechDTO> speechesDTO = new ArrayList<>();

        for(Speech speech : speeches) {

            String speechId = speech.getSpeechId();
            String dockDate = speech.getDockDate();
            String speaker = MemberHelper.removeTitle(speech.getSpeaker());
            String party = Optional.ofNullable(speech.getParty()).orElse("");

            Member member = Optional.ofNullable(membersSpeech.get(speechId)).orElse(new Member());

            String officialEmail = Optional.ofNullable(member.extractOfficialEmail()).orElse("");
            String constituency = Optional.ofNullable(member.extractConstituency()).orElse("");
            String urlImage = Optional.ofNullable(member.extractUrlImage()).orElse("");
            String debateSubject = Optional.ofNullable(speech.getSectionHeading()).orElse("");

            Link selfLink = new Link("http://data.riksdagen.se/anforande/" + speech.getDockId() + "-" +
                    speech.getSpeechNumber());

            speechesDTO.add(new SpeechDTO(speechId, dockDate, speaker, party, officialEmail, constituency, urlImage,
                    debateSubject, selfLink));
        }

        return speechesDTO;
    }
}
