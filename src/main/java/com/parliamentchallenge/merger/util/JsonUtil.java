package com.parliamentchallenge.merger.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;

import java.util.EnumSet;
import java.util.Set;

public class JsonUtil {
    public static String getFieldValue(String source, String evaluator, String field) {
        Configuration.setDefaults(new Configuration.Defaults() {

            private final JsonProvider jsonProvider = new JacksonJsonProvider();
            private final MappingProvider mappingProvider = new JacksonMappingProvider();

            @Override
            public JsonProvider jsonProvider() {
                return jsonProvider;
            }

            @Override
            public MappingProvider mappingProvider() {
                return mappingProvider;
            }

            @Override
            public Set<Option> options() {
                return EnumSet.noneOf(Option.class);
            }
        });

        JsonNode taskMemberNode = JsonPath.parse(source).read(evaluator, JsonNode.class);

        if(taskMemberNode instanceof TextNode) {
            return taskMemberNode.asText();
        } else if (taskMemberNode instanceof ArrayNode) {
            if (taskMemberNode.get(0) != null && taskMemberNode.get(0).get(field) != null &&
                    taskMemberNode.get(0).get(field).get(0) != null) {

            }
            return (taskMemberNode.get(0) != null && taskMemberNode.get(0).get(field) != null &&
                    taskMemberNode.get(0).get(field).get(0) != null) ?
                    taskMemberNode.get(0).get(field).get(0).asText() : "";
        }

        return "";
    }
}
