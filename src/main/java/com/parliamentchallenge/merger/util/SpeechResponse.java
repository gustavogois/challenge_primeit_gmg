package com.parliamentchallenge.merger.util;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SpeechResponse {

    @JsonProperty("anforandelista")
    private SpeechResponseElement speechResponseElement;

    public SpeechResponse() {
    }

    public SpeechResponse(SpeechResponseElement speechResponseElement) {
        this.speechResponseElement = speechResponseElement;
    }

    public SpeechResponseElement getSpeechResponseElement() {
        return speechResponseElement;
    }

    public void setSpeechResponseElement(SpeechResponseElement speechResponseElement) {
        this.speechResponseElement = speechResponseElement;
    }
}
