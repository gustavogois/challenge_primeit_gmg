package com.parliamentchallenge.merger.service.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.parliamentchallenge.merger.service.model.Member;
import com.parliamentchallenge.merger.service.model.Speech;
import com.parliamentchallenge.merger.util.SpeechResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Component
public class SpeechRestDAOImpl implements SpeechDAO {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ObjectMapper objectMapper;

    public SpeechRestDAOImpl() {
    }

    public SpeechRestDAOImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<Speech> loadTenLastSpeeches() throws IOException {

        String urlSpeech = "http://data.riksdagen.se/anforandelista/?anftyp=Nej&sz=10&utformat=json";
        String json = restTemplate.getForObject(urlSpeech, String.class);

        SpeechResponse speechResponse = objectMapper.readerFor(SpeechResponse.class).readValue(json);

        return speechResponse.getSpeechResponseElement().getSpeedList();

    }

    @Override
    public Member findMemberSpeech(String stakeholderId) {

        String urlMember = "http://data.riksdagen.se/personlista/?iid=" + stakeholderId +"&utformat=json";
        String json = restTemplate.getForObject(urlMember, String.class);

        Member member = Member.createFromJson(json);

        return member;
    }
}
