package com.parliamentchallenge.merger.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.parliamentchallenge.merger.dto.SpeechesDTO;
import com.parliamentchallenge.merger.service.ServicesNames;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.MimeTypeUtils;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SpeechControllerIT {

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void retrieveLastTenSpeeches() throws Exception {
        MvcResult mvcResult = mvc.perform(
                get(ServicesNames.RETRIEVE_LAST_TEN_SPEECHES).accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
                .andReturn();
        String contentAsString = mvcResult.getResponse().getContentAsString();
        SpeechesDTO speechesDTO = objectMapper.readValue(contentAsString, SpeechesDTO.class);

        Integer speechListSize = Optional.ofNullable(speechesDTO.getSpeechesDTOS().size())
                .get();
        assertThat(speechListSize).isEqualTo(10);

    }
}
